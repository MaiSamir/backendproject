package com.example.foodyApp.menuItem;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.example.foodyApp.exceptions.WrongFormatException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityNotFoundException;

class MenuItemServiceTest {
	MenuItemService menuItemService;

	@BeforeEach
	void init(){
		menuItemService=new MenuItemService();
	}

	@Test
	void testInValidMenuItems() {
		//menu item with no name
		MenuItem noNameMenuItem=new MenuItem();
		noNameMenuItem.setPrice(10);
		noNameMenuItem.setDescription("desc");
		noNameMenuItem.setType("type");

		//menu item with zero price

		MenuItem noPriceMenuItem=new MenuItem();
		noPriceMenuItem.setName("name");
		noPriceMenuItem.setPrice(0);
		noPriceMenuItem.setDescription("desc");
		noPriceMenuItem.setType("type");

		//menu item with no description
		MenuItem noDescriptionMenuItem=new MenuItem();
		noDescriptionMenuItem.setName("name");
		noDescriptionMenuItem.setPrice(10);
		noDescriptionMenuItem.setType("type");

		//menu item with no attributes
		MenuItem noAttributesMenuItem= new MenuItem();



		assertAll(
				()->assertThrows(WrongFormatException.class,()->menuItemService.isValid(noNameMenuItem)),
				()->assertThrows(WrongFormatException.class,()->menuItemService.isValid(noPriceMenuItem)),
				()->assertThrows(WrongFormatException.class,()->menuItemService.isValid(noDescriptionMenuItem)),
				()->assertThrows(WrongFormatException.class,()->menuItemService.isValid(noAttributesMenuItem))

		);

	}


	@Test
	void testUpdateNonExistingMenuItem() {
		MenuItemRepository mockMenuItemRepository= mock(MenuItemRepository.class);
		when(mockMenuItemRepository.findById(17)).thenReturn(null);
		menuItemService.setMenuItemRepository(mockMenuItemRepository);

		assertThrows(EntityNotFoundException.class,()->menuItemService.updateMenuItem(17,new MenuItem()));

	}

	@Test
	void testAddInvalidMenuItem() {

		//menu item with no name
		MenuItem noNameMenuItem=new MenuItem();
		noNameMenuItem.setPrice(10);
		noNameMenuItem.setDescription("desc");
		noNameMenuItem.setType("type");

		assertThrows(WrongFormatException.class,()->menuItemService.addMenuItem(noNameMenuItem));
	}

	@Test
	void testDeleteNonExistingMenuItem() {

		MenuItemRepository mockMenuItemRepository= mock(MenuItemRepository.class);
		when(mockMenuItemRepository.existsById(17)).thenReturn(false);
		menuItemService.setMenuItemRepository(mockMenuItemRepository);

		assertThrows(EntityNotFoundException.class,()->menuItemService.deleteMenuItem(17));
	}

}
