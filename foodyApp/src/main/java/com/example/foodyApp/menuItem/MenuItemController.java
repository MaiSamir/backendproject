package com.example.foodyApp.menuItem;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
@RestController
public class MenuItemController implements MenuItemControllerInterface {
    @Autowired
    private MenuItemService menuItemService;


    @Override
    public ResponseEntity<List<MenuItem>> getMenuItems() {

        return ResponseEntity.ok(menuItemService.getAllMenuItems());
    }

    @Override
    public ResponseEntity<MenuItem> getMenuItemById(int id) {
        return ResponseEntity.ok(menuItemService.getMenuItem(id));
    }

    @Override
    public ResponseEntity<?> addMenuItem(MenuItem menuItem, UriComponentsBuilder b) {
        MenuItem newMenuItem= menuItemService.addMenuItem(menuItem);
        UriComponents uriComponents =
                b.path("/menu/{id}").buildAndExpand(newMenuItem.getMenuItem_id());
        return ResponseEntity.created(uriComponents.toUri()).body(newMenuItem);
    }

    @Override
    public ResponseEntity<?> updateMenuItem(int id, MenuItem menuItem) {

        return ResponseEntity.ok(menuItemService.updateMenuItem(id,menuItem));
    }

    @Override
    public ResponseEntity<?> deleteMenuItem(int id) {
        menuItemService.deleteMenuItem(id);
        return ResponseEntity.ok().build();
    }
}
