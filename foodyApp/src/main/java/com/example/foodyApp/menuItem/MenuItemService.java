package com.example.foodyApp.menuItem;

import com.example.foodyApp.exceptions.WrongFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MenuItemService  {


    @Autowired
    private MenuItemRepository menuItemRepository;

    public void setMenuItemRepository(MenuItemRepository menuItemRepository) {
        this.menuItemRepository = menuItemRepository;
    }
    public void isValid(MenuItem menuItem){
        if(menuItem.getName()==null)
            throw new WrongFormatException("Menu Item missing name field. Menu item should contain name,description,type,and price fields.");
        if(menuItem.getDescription()==null)
            throw new WrongFormatException("Menu Item missing description field. Menu item should contain name,description,type,and price fields.");
        if(menuItem.getPrice()==0)
            throw new WrongFormatException("Menu Item price missing. Menu item should contain name,description,type,and price fields.");
    }

    public List<MenuItem> getAllMenuItems(){
        return new ArrayList<>(menuItemRepository.findAll());
    }

    public MenuItem getMenuItem(int id){
        Optional<MenuItem> menuItem= menuItemRepository.findById(id);
        menuItem.orElseThrow(()-> new EntityNotFoundException("Menu Item with id: "+id +" not found."));

        return menuItem.get();
    }

    public MenuItem updateMenuItem(int id,MenuItem menuItem){
        //updating non existing menu item
        if(!menuItemRepository.existsById(id))
            throw new EntityNotFoundException("Menu Item with id: "+id+" not found.");
        //check if menu item provided with correct fields
        isValid(menuItem);
        return menuItemRepository.save(menuItem);
    }
    public MenuItem addMenuItem(MenuItem menuItem){
        //check if id provided and item exists
        if(menuItem.getMenuItem_id()!=0)
        if(menuItemRepository.existsById(menuItem.getMenuItem_id()))
            throw new WrongFormatException("Menu Item with id: "+menuItem.getMenuItem_id()+" already exists.");
        //check if menu item provided with correct fields
        isValid(menuItem);
        return menuItemRepository.save(menuItem);
    }
    public void deleteMenuItem(int id){
        //deleting non existing menu item
        if(!menuItemRepository.existsById(id))
            throw new EntityNotFoundException("Menu Item with id: "+id+" not found.");
         menuItemRepository.deleteById(id);
    }





}
