package com.example.foodyApp.menuItem;

import com.example.foodyApp.order.orderItem.OrderItem;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "MenuItem")
public class MenuItem {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int menuItem_id;
    private String name;
    private String description;
    private double price;
    private String type;
    private String imageURL;



    public MenuItem(){}

    public MenuItem(int menuItem_id, String name, String description, double price, String type, String imageURL) {
        this.menuItem_id = menuItem_id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.type = type;
        this.imageURL = imageURL;
    }

    public int getMenuItem_id() {
        return menuItem_id;
    }

    public void setMenuItem_id(int menuItem_id) {
        this.menuItem_id = menuItem_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }


}
