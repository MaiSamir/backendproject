package com.example.foodyApp.menuItem;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;




public interface MenuItemControllerInterface {


    @GetMapping("/menu")
    ResponseEntity<?> getMenuItems();

    @GetMapping("/menu/{id}")
    ResponseEntity<?> getMenuItemById(@PathVariable int id);

    @PostMapping("/admin/menu")
    ResponseEntity<?> addMenuItem(@RequestBody MenuItem menuItem, UriComponentsBuilder b);

    @PutMapping("/admin/menu/{id}")
    ResponseEntity<?> updateMenuItem(@PathVariable  int id, @RequestBody MenuItem menuItem);

    @DeleteMapping("/admin/menu/{id}")
    ResponseEntity<?> deleteMenuItem(@PathVariable int id);






}
