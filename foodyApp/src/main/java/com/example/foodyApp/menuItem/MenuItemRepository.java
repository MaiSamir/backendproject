package com.example.foodyApp.menuItem;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MenuItemRepository extends JpaRepository<MenuItem, Integer> {


    public List<MenuItem> findByName(String name);


}
