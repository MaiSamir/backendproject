package com.example.foodyApp;

import com.example.foodyApp.order.OrderRepository;
import com.example.foodyApp.user.UserRepository;
import com.example.foodyApp.menuItem.MenuItemRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackageClasses = {UserRepository.class, MenuItemRepository.class, OrderRepository.class})
@EntityScan

public class FoodyAppApplication {
	public static final Logger log =
			LoggerFactory.getLogger(FoodyAppApplication.class);

	public static void main(String[] args) {

		log.info("Starting Application");
		SpringApplication.run(FoodyAppApplication.class, args);
		log.info("Application Started Successfully") ;
	}

}
