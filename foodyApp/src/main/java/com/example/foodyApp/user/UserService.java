package com.example.foodyApp.user;

import com.example.foodyApp.authentication.JwtUtil;
import com.example.foodyApp.exceptions.WrongFormatException;
import com.example.foodyApp.order.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private JwtUtil jwtUtil;

    public boolean isValid(User user){
        if(user.getEmail()==null)
            throw new WrongFormatException("User should have email");
                if( user.getPassword()==null)
                    throw new WrongFormatException("User should have a password");
                if(user.getFirstName()==null)
                    throw new WrongFormatException("User should have a first name");
                if(user.getLastName()==null)
                    throw new WrongFormatException("User should have a last name");
                if(user.getAddress()==null)
                    throw new WrongFormatException("User should have an address");

                if(user.getPhoneNumber()==null)
                    throw new WrongFormatException("User should have a phone number");

                return true;


    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user=userRepository.findById(username);
        user.orElseThrow(()->new UsernameNotFoundException("Not Found:" +username));

        return user.map(FoodyUserDetails::new).get();
    }
    public User register(User user){
        if(userRepository.existsById(user.getEmail()))
            throw new WrongFormatException("User email already exists");
        user.setRole("USER");
        isValid(user);


        return userRepository.save(user);

    }
    public User registerAdmin (User user){
        if(userRepository.existsById(user.getEmail()))
            throw new WrongFormatException("Admin email already exists");
        user.setRole("ADMIN");
        isValid(user);


        return userRepository.save(user);

    }
    public List<User> getAllUsers(){
        if(SecurityContextHolder.getContext().getAuthentication()
                .getAuthorities().contains("ADMIN"))
        return new ArrayList<>(userRepository.findAll());
        throw new AuthorizationServiceException("not an admin");
    }

    public void logout(){

        //get token
        //expire token


    }
    public User getUserById(String email){

        return userRepository.findById(email).get();
    }

    public User updateUser(User user){
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        String userEmail = userDetails.getUsername();

        user.setEmail(userEmail);
        isValid(user);

        return userRepository.save(user);
    }
    public List<Order> userOrderHistory(){
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        String userEmail = userDetails.getUsername();
        User user= userRepository.findById(userEmail).get();
        List<Order> orderHistory=new ArrayList<>();
        for (Order order:user.getOrders()) {
            Order addOrder=new Order();
            addOrder.setOrderItems(order.getOrderItems());
            addOrder.setTotalPrice(order.getTotalPrice());
            addOrder.setStatus(order.getStatus());
            addOrder.setId(order.getId());
            orderHistory.add(addOrder);
        }
        return  orderHistory;
    }
    public void deleteUser() {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        String userEmail = userDetails.getUsername();
        userRepository.deleteById(userEmail);

    }

    public void deleteUserbyId(String email){

        if(!userRepository.existsById(email))
            throw new EntityNotFoundException("User with email "+email+" not found");


        userRepository.deleteById(email);

        }




}
