package com.example.foodyApp.user;

import com.example.foodyApp.menuItem.MenuItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
public class UserController implements UserControllerInterface{

    @Autowired
    private UserService userService;


    @Override
    public ResponseEntity<?> getAllUsers() {
        return ResponseEntity.ok(userService.getAllUsers());
    }

    @Override
    public ResponseEntity<?> register(User user, UriComponentsBuilder b) {
        User newUser= userService.register(user);
        UriComponents uriComponents =
                b.path("/user/{id}").buildAndExpand(newUser.getEmail());
        return ResponseEntity.created(uriComponents.toUri()).body(newUser);
    }

    @Override
    public ResponseEntity<?> registerAdmin(User user, UriComponentsBuilder b) {
        User newAdmin= userService.register(user);
        UriComponents uriComponents =
                b.path("/admin/{id}").buildAndExpand(newAdmin.getEmail());
        return ResponseEntity.created(uriComponents.toUri()).body(newAdmin);
    }

    @Override
    public ResponseEntity<?> logout() {
        return null;
    }

    @Override
    public ResponseEntity<?> getOrderHistory() {
        return ResponseEntity.ok(userService.userOrderHistory());
    }

    @Override
    public ResponseEntity<?> deleteUserById(String email) {
        userService.deleteUserbyId(email);

        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<?> deleteUser() {
        userService.deleteUser();
       return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<?> updateUser(User user) {
        return ResponseEntity.ok(userService.updateUser(user));
    }


}
