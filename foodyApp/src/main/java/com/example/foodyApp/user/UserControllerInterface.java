package com.example.foodyApp.user;


import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

public interface UserControllerInterface {


    @GetMapping("/admin/users")
    ResponseEntity<?> getAllUsers();

    @PostMapping("/register")
    ResponseEntity<?> register(@Validated @RequestBody User user, UriComponentsBuilder b);
    @PostMapping("/admin/register")
    ResponseEntity<?> registerAdmin(@Validated @RequestBody User user, UriComponentsBuilder b);

    @GetMapping("/user")
    ResponseEntity<?> logout();

    @GetMapping("/user/order")
    ResponseEntity<?> getOrderHistory();



    @DeleteMapping("/admin/user/{email}")
    ResponseEntity<?> deleteUserById(@PathVariable String email);

    @DeleteMapping("/user")
    ResponseEntity<?> deleteUser();


    @PutMapping("/user")
    ResponseEntity<?> updateUser(@Validated @RequestBody User user);








}
