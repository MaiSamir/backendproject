package com.example.foodyApp.order.orderItem;

import com.example.foodyApp.menuItem.MenuItem;
import com.example.foodyApp.order.Order;

import javax.persistence.*;

@Entity
public class OrderItem {
    @EmbeddedId
    private OrderItemCompositeKey id;

    @ManyToOne
    @MapsId("id")
    @JoinColumn(name="menuItem_id")
    private MenuItem menuItem;

    @ManyToOne
    @MapsId("id")
    @JoinColumn(name="order_id")
    private Order order;

    private int quantity;

    public OrderItem(){}
    public OrderItem(OrderItemCompositeKey id, int quantity) {
        this.id=id;

        this.quantity = quantity;
    }

    public OrderItemCompositeKey getId() {
        return id;
    }

    public void setId(OrderItemCompositeKey id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
