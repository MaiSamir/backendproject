package com.example.foodyApp.order.orderItem;

import com.example.foodyApp.menuItem.MenuItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderItemRepository extends JpaRepository<OrderItem,OrderItemCompositeKey> {
    public List<MenuItem> findByOrderId(int orderId);
}
