package com.example.foodyApp.order;

import com.example.foodyApp.FoodyAppApplication;
import com.example.foodyApp.exceptions.WrongFormatException;
import com.example.foodyApp.menuItem.MenuItem;
import com.example.foodyApp.menuItem.MenuItemService;
import com.example.foodyApp.order.orderItem.OrderItem;
import com.example.foodyApp.order.orderItem.OrderItemCompositeKey;
import com.example.foodyApp.order.orderItem.OrderItemService;
import com.example.foodyApp.user.User;
import com.example.foodyApp.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.*;

@Service
public class OrderService {
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private MenuItemService menuItemService;
    @Autowired
    private UserService userService;
    @Autowired
    private OrderItemService orderItemService;

    public List<Order> getAllOrders(){
        return new ArrayList<>(orderRepository.findAll());
    }

    public void setOrderTotalPrice(Order order){
        List<OrderItem> orderItems=  new ArrayList<OrderItem>(order.getOrderItems());
        if(orderItems==null){
            order.setTotalPrice(0.0);
            return;
        }
        double totalPrice=0;
        for(int i=0;i<orderItems.size();i++){
            OrderItem orderItem= orderItems.get(i);
            totalPrice += (menuItemService.getMenuItem(orderItem.getId().getMenuItem_id()).getPrice()*orderItem.getQuantity());
        }
        order.setTotalPrice(totalPrice);

    }

    public Order getOrder(int id){
        Optional<Order> order= orderRepository.findById(id);
        order.orElseThrow(()-> new EntityNotFoundException("Order with id: "+id +" not found."));

        return order.get();
    }

    public Order updateOrder(int id,Order order){
        //updating non existing menu item
        if(!orderRepository.existsById(id))
            throw new EntityNotFoundException("Order with id: "+id+" not found.");
        //check if menu item provided with correct fields
        //isValid(menuItem);
        setOrderTotalPrice(order);
        return orderRepository.save(order);
    }
    public Order checkOut(Map<Integer, Integer> menuItems){
        FoodyAppApplication.log.info("Checkout");
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        String userEmail = userDetails.getUsername();
        FoodyAppApplication.log.info("USER: "+userEmail);
        User user= userService.getUserById(userEmail);

        Order newOrder=orderRepository.save(new Order(0,"CHECKOUT",user));
        newOrder.setOrderItems(new HashSet<OrderItem>());

       // Set<OrderItem> orderItems=newOrder.getOrderItems();
           for (int menuItemid:menuItems.keySet()) {
                FoodyAppApplication.log.info("ITEM ID: "+menuItemid);
                MenuItem menuItem= menuItemService.getMenuItem(menuItemid);
                OrderItemCompositeKey orderItemKey= new OrderItemCompositeKey(newOrder.getId(), menuItemid);
                int quantity=menuItems.get(menuItemid);
               OrderItem orderItem=new OrderItem(orderItemKey,quantity);

            orderItemService.addOrderItem(orderItem);
            newOrder.getOrderItems().add(orderItem);
              // FoodyAppApplication.log.info("ORDERITEMS:2 "+orderItems);

        }

          //  newOrder.setOrderItems(orderItems);
        setOrderTotalPrice(newOrder);

            return orderRepository.save( newOrder);





    }
    public Order addOrder(Order order){

        //check if menu item provided with correct fields
        //isValid(menuItem);
        setOrderTotalPrice(order);
        return orderRepository.save(order);
    }
    public void deleteOrder(int id){
        //deleting non existing menu item
        if(!orderRepository.existsById(id))
            throw new EntityNotFoundException("Order with id: "+id+" not found.");
        orderRepository.deleteById(id);
    }

}
