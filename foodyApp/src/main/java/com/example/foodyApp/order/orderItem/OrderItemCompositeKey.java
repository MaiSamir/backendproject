package com.example.foodyApp.order.orderItem;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class OrderItemCompositeKey implements Serializable {

        @Column(name = "order_id")
        private int order_id;

        @Column(name = "menuItem_id")
        private int menuItem_id;

    public OrderItemCompositeKey() {
    }

    public OrderItemCompositeKey(int order_id, int product_id) {
        this.order_id = order_id;
        this.menuItem_id = product_id;
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public int getMenuItem_id() {
        return menuItem_id;
    }

    public void setMenuItem_id(int menuItem_id) {
        this.menuItem_id = menuItem_id;
    }

    // hashcode and equals implementation

}
