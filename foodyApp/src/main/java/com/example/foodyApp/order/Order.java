package com.example.foodyApp.order;

import com.example.foodyApp.order.orderItem.OrderItem;
import com.example.foodyApp.user.User;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "Order_details")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private double totalPrice;

    private String status;


    @ManyToOne
    @JoinColumn(name = "user_email")
    private User user;

    @OneToMany(mappedBy = "order")
    Set<OrderItem> orderItems;
    public Order(){}
    public Order( double totalPrice, String status, User user) {

        this.totalPrice = totalPrice;
        this.status = status;
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(Set<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
