package com.example.foodyApp.order;

import com.example.foodyApp.menuItem.MenuItem;
import com.example.foodyApp.order.orderItem.OrderItemController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Map;

@RestController
public class OrderController implements OrderControllerInterface {
    @Autowired
    private OrderService orderService;


    @Override
    public ResponseEntity<?> getOrders() {
        return null;
    }

    @Override
    public ResponseEntity<?> getOrderById(int id) {
        return null;
    }

    @Override
    public ResponseEntity<?> addOrder(Order order, UriComponentsBuilder b) {
        return null;
    }

    @Override
    public ResponseEntity<?> updateOrder(int id, Order order) {
        return null;
    }

    @Override
    public ResponseEntity<?> deleteOrder(int id) {
        return null;
    }

    @Override
    public ResponseEntity<?> checkout(Map<Integer, Integer> menuItems) {
        return ResponseEntity.ok(orderService.checkOut(menuItems));
    }




}
