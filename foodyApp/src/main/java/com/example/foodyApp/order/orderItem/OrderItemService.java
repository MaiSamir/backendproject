package com.example.foodyApp.order.orderItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class OrderItemService {
    @Autowired
    private OrderItemRepository orderItemRepository;



    public List<OrderItem> getAllOrderItems(){
        return new ArrayList<> (orderItemRepository.findAll());
    }


    public OrderItem getOrderItem(OrderItemCompositeKey orderItemId){
       // OrderItemCompositeKey id= new OrderItemCompositeKey(orderId,menuItemId);
        Optional<OrderItem> orderItem= orderItemRepository.findById(orderItemId);
        orderItem.orElseThrow(()->new EntityNotFoundException("Menu Item with id: "+orderItemId +" not found."));
        return orderItem.get() ;

    }
    public OrderItem updateOrderItem(OrderItem orderItem){
        //updating non existing menu item
        if(!orderItemRepository.existsById(orderItem.getId()))
            throw new EntityNotFoundException("Order Item with id: "+orderItem.getId()+" not found.");
        //check if menu item provided with correct fields
        //isValid(menuItem);
        return orderItemRepository.save(orderItem);
    }
    public OrderItem addOrderItem(OrderItem orderItem){
        //check if id provided and item exists
        //if(orderItem.getMenuItem_id()!=0)
          //  if(menuItemRepository.existsById(menuItem.getMenuItem_id()))
            //    throw new WrongFormatException("Menu Item with id: "+menuItem.getMenuItem_id()+" already exists.");
        //check if menu item provided with correct fields
      //  isValid(menuItem);
        return orderItemRepository.save(orderItem);
    }
    public void deleteOrderItem(OrderItemCompositeKey id){
        //deleting non existing menu item
        if(!orderItemRepository.existsById(id))
            throw new EntityNotFoundException("Menu Item with id: "+id+" not found.");
        orderItemRepository.deleteById(id);
    }




}
