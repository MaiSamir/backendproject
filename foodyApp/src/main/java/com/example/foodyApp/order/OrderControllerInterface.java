package com.example.foodyApp.order;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Map;

public interface OrderControllerInterface {



    @GetMapping("/orders")
    ResponseEntity<?> getOrders();

    @GetMapping("/orders/{id}")
    ResponseEntity<?> getOrderById(@PathVariable int id);

    @PostMapping("/orders")
    ResponseEntity<?> addOrder(@RequestBody Order order, UriComponentsBuilder b);

    @PutMapping("/orders/{id}")
    ResponseEntity<?> updateOrder(@PathVariable int id, @RequestBody Order order);

    @DeleteMapping("/orders/{id}")
    ResponseEntity<?> deleteOrder(@PathVariable int id);

    @PostMapping("/checkout")
    ResponseEntity<?> checkout(@RequestBody Map<Integer,Integer> menuItems);

}
