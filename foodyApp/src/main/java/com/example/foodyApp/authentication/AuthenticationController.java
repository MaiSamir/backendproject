package com.example.foodyApp.authentication;

import com.example.foodyApp.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthenticationController implements AuthenticationControllerInterface {
   @Autowired
   private AuthenticationManager authenticationManager;
   @Autowired
   private UserService userService;
   @Autowired
   private JwtUtil jwtUtil;

    @Override
    public ResponseEntity<?> createAuthenticationToken(AuthenticationRequest authenticationRequest) {
        authenticationManager.authenticate(
          new UsernamePasswordAuthenticationToken(
                  authenticationRequest.getEmail(),authenticationRequest.getPassword()
          )
        );
        final UserDetails userDetails=userService.loadUserByUsername(authenticationRequest.getEmail());
        final String jwt=jwtUtil.generateToken(userDetails);

        return ResponseEntity.ok(new AuthenticationResponse(jwt));

    }

    @Override
    public ResponseEntity<?> logout() {
        return null;
    }
}
