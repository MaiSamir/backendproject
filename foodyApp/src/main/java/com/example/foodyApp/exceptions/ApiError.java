package com.example.foodyApp.exceptions;

import org.springframework.http.HttpStatus;

public class ApiError {
    private int error;
    private HttpStatus httpStatus;
    private String message;

    public ApiError(HttpStatus httpStatus) {
        this.error=httpStatus.value();
        this.httpStatus=httpStatus;

    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
