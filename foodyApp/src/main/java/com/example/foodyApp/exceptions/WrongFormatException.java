package com.example.foodyApp.exceptions;

public class WrongFormatException extends RuntimeException{
    public WrongFormatException(String message) {
        super(message);
    }
}
